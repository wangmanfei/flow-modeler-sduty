package com.haiyang.flowable.listener;

import org.flow.config.DatabaseConfiguration;
import org.flowable.engine.delegate.TaskListener;
import org.flowable.task.service.delegate.DelegateTask;

public class ManagerTaskHandler implements TaskListener {

    @Override
    public void notify(DelegateTask delegateTask) {
        Object userId = delegateTask.getVariable("userId");
        delegateTask.setAssignee("经理"+userId);
    }

}