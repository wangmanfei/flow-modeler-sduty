package org.flow.main;

import org.flow.config.ApplicationConfiguration;
import org.flow.servlet.AppDispatcherServletConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @Description:
 * @Author: Bruce.liu
 * @Since:9:18 2019/1/19
 */
@Import({
        ApplicationConfiguration.class,
        AppDispatcherServletConfiguration.class
})
@SpringBootApplication
@ComponentScan(basePackages = {"org.flow","com.haiyang.flowable.listener"})
@EnableTransactionManagement
public class FlowApplication {

    public static void main(String[] args) {
        SpringApplication.run(FlowApplication.class, args);
        System.out.println("配置流程登陆页面：http://127.0.0.1:8989/flow-study/idm/index.html");
        System.out.println("配置流程页：http://127.0.0.1:8989/flow-study/");
        System.out.print("程序正在运行。。。。。。。。。。。。。");
    }
}
